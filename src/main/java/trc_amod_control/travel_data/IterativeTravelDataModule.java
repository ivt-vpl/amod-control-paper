package trc_amod_control.travel_data;

import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Population;
import org.matsim.core.controler.AbstractModule;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import ch.ethz.idsc.amodeus.options.ScenarioOptions;
import ch.ethz.idsc.amodeus.traveldata.TravelData;
import ch.ethz.idsc.amodeus.virtualnetwork.VirtualNetwork;
import ch.ethz.matsim.av.framework.AVModule;

public class IterativeTravelDataModule extends AbstractModule {
	private final int numberOfVehicles;
	private final ScenarioOptions scenarioOptions;

	public IterativeTravelDataModule(int numberOfVehicles, ScenarioOptions scenarioOptions) {
		this.numberOfVehicles = numberOfVehicles;
		this.scenarioOptions = scenarioOptions;
	}

	@Override
	public void install() {
		addEventHandlerBinding().to(RequestTracker.class);
		addControlerListenerBinding().to(IterativeTravelData.class);
		bind(TravelData.class).to(IterativeTravelData.class);
	}

	@Provides
	@Singleton
	public RequestTracker provideRequestTracker(Population population, @Named(AVModule.AV_MODE) Network network) {
		return new RequestTracker(population, network);
	}

	@Provides
	@Singleton
	public IterativeTravelData provideIterativeTravelData(RequestTracker tracker,
			@Named(AVModule.AV_MODE) Network network, VirtualNetwork<Link> virtualNetwork) {
		return new IterativeTravelData(tracker, network, scenarioOptions, virtualNetwork, numberOfVehicles);
	}
}
