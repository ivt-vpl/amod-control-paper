package trc_amod_control.travel_data;

import java.util.Set;

import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.listener.IterationEndsListener;

import ch.ethz.idsc.amodeus.lp.LPPreparer;
import ch.ethz.idsc.amodeus.lp.LPSolver;
import ch.ethz.idsc.amodeus.options.ScenarioOptions;
import ch.ethz.idsc.amodeus.prep.PopulationTools;
import ch.ethz.idsc.amodeus.prep.Request;
import ch.ethz.idsc.amodeus.traveldata.TravelData;
import ch.ethz.idsc.amodeus.virtualnetwork.VirtualNetwork;
import ch.ethz.idsc.tensor.Tensor;
import ch.ethz.idsc.tensor.Tensors;

public class IterativeTravelData extends TravelData implements IterationEndsListener {
	private final RequestTracker tracker;
	private final Network network;
	private final ScenarioOptions scenarioOptions;
	private final VirtualNetwork<Link> virtualNetwork;
	private final int numberOfVehicles;

	private TravelData currentTravelData;

	/**
	 * This is a bit ugly. Ideally, TravelData should be an interface!
	 */
	private final static Tensor fakeTensor = Tensors.fromString("{{{ 1 }}})");

	public IterativeTravelData(RequestTracker tracker, Network network, ScenarioOptions scenarioOptions,
			VirtualNetwork<Link> virtualNetwork, int numberOfVehicles) {
		super(0, fakeTensor, fakeTensor, fakeTensor, fakeTensor, "");

		this.tracker = tracker;
		this.network = network;
		this.scenarioOptions = scenarioOptions;
		this.virtualNetwork = virtualNetwork;
		this.numberOfVehicles = numberOfVehicles;

		updateTravelData();
	}

	@Override
	public void notifyIterationEnds(IterationEndsEvent event) {
		updateTravelData();
	}

	private void updateTravelData() {
		try {
			// From TravelDataCreator::getLambdaAbsolute

			Set<Request> avRequests = tracker.getRequestsAndReset();
			Tensor lambdaAbsolute = PopulationTools.getLambdaInVirtualNodesAndTimeIntervals(avRequests, virtualNetwork,
					scenarioOptions.getdtTravelData());

			// From TravelDataCreator::create

			LPSolver lp = LPPreparer.run(virtualNetwork, network, lambdaAbsolute, scenarioOptions, numberOfVehicles);

			String lpName = lp.getClass().getSimpleName();
			Tensor alphaAbsolute = lp.getAlphaAbsolute_ij();
			Tensor v0_i = lp.getV0_i();
			Tensor fAbsolute = lp.getFAbsolute_ij();

			currentTravelData = new TravelData(virtualNetwork.getvNetworkID(), lambdaAbsolute, alphaAbsolute, fAbsolute,
					v0_i, lpName);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public int hashCode() {
		return currentTravelData.hashCode();
	}

	public Tensor getLambdaAbsoluteAtTime(int time) {
		return currentTravelData.getLambdaAbsoluteAtTime(time);
	}

	public Tensor getLambdaAbsolute() {
		return currentTravelData.getLambdaAbsolute();
	}

	public Tensor getLambdaRateAtTime(int time) {
		return currentTravelData.getLambdaRateAtTime(time);
	}

	public Tensor getAlphaAbsolute() {
		return currentTravelData.getAlphaAbsolute();
	}

	public boolean equals(Object obj) {
		return currentTravelData.equals(obj);
	}

	public Tensor getAlphaAbsoluteAtTime(int time) {
		return currentTravelData.getAlphaAbsoluteAtTime(time);
	}

	public Tensor getAlphaRate() {
		return currentTravelData.getAlphaRate();
	}

	public Tensor getAlphaRateAtTime(int time) {
		return currentTravelData.getAlphaRateAtTime(time);
	}

	public Tensor getFAbsolute() {
		return currentTravelData.getFAbsolute();
	}

	public Tensor getFAbsoluteAtTime(int time) {
		return currentTravelData.getFAbsoluteAtTime(time);
	}

	public Tensor getFRate() {
		return currentTravelData.getFRate();
	}

	public Tensor getFRateAtTime(int time) {
		return currentTravelData.getFRateAtTime(time);
	}

	public Tensor getV0() {
		return currentTravelData.getV0();
	}

	public Tensor getLambdaRate() {
		return currentTravelData.getLambdaRate();
	}

	public long getVirtualNetworkID() {
		return currentTravelData.getVirtualNetworkID();
	}

	public int getTimeSteps() {
		return currentTravelData.getTimeSteps();
	}

	public int getTimeIntervalLength() {
		return currentTravelData.getTimeIntervalLength();
	}

	public String getLPName() {
		return currentTravelData.getLPName();
	}

	public boolean coversTime(long round_now) {
		return currentTravelData.coversTime(round_now);
	}

	public void checkIdenticalVirtualNetworkID(long virtualNetworkID) {
		currentTravelData.checkIdenticalVirtualNetworkID(virtualNetworkID);
	}

	public String toString() {
		return currentTravelData.toString();
	}
}
