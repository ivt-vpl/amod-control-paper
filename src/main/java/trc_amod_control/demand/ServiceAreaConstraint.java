package trc_amod_control.demand;

import java.util.BitSet;
import java.util.Collection;
import java.util.List;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;

import ch.ethz.matsim.mode_choice.v2.framework.ModeChoiceTrip;
import ch.ethz.matsim.mode_choice.v2.framework.tour_based.constraints.TourConstraint;
import ch.ethz.matsim.mode_choice.v2.framework.tour_based.constraints.TourConstraintFactory;
import ch.ethz.matsim.mode_choice.v2.framework.tour_based.estimation.TourCandidate;

public class ServiceAreaConstraint implements TourConstraint {
	private final BitSet viableMask;

	private ServiceAreaConstraint(BitSet viableMask) {
		this.viableMask = viableMask;
	}

	@Override
	public boolean validateAfterEstimation(TourCandidate candidate, List<TourCandidate> previousCandidates) {
		return true;
	}

	@Override
	public boolean validateBeforeEstimation(List<String> modes, List<List<String>> previousModes) {
		for (int i = 0; i < modes.size(); i++) {
			if (modes.get(i).equals("av")) {
				if (!viableMask.get(i)) {
					return false;
				}
			}
		}

		return true;
	}

	public static class Factory implements TourConstraintFactory {
		private final Collection<Id<Link>> viableLinkIds;

		public Factory(Collection<Id<Link>> viableLinkIds) {
			this.viableLinkIds = viableLinkIds;
		}

		@Override
		public TourConstraint createConstraint(List<ModeChoiceTrip> trips, Collection<String> initialModes) {
			BitSet viableMask = new BitSet(trips.size());

			for (int i = 0; i < trips.size(); i++) {
				Id<Link> startLinkid = trips.get(i).getTripInformation().getOriginActivity().getLinkId();
				Id<Link> endLinkId = trips.get(i).getTripInformation().getDestinationActivity().getLinkId();

				viableMask.set(i, viableLinkIds.contains(startLinkid) && viableLinkIds.contains(endLinkId));
			}

			return new ServiceAreaConstraint(viableMask);
		}
	}
}
