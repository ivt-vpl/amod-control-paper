package trc_amod_control.analysis.traces;

import java.io.File;

import org.matsim.api.core.v01.network.Network;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.events.EventsReaderXMLv1;
import org.matsim.core.events.EventsUtils;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.core.utils.io.UncheckedIOException;

import ch.ethz.matsim.av.schedule.AVTransitEventMapper;
import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;

public class ConvertAVTracesFromEvents {
	static public void main(String[] args) throws UncheckedIOException, ConfigurationException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("events-path", "network-path", "output-path") //
				.build();

		Network network = NetworkUtils.createNetwork();
		new MatsimNetworkReader(network).readFile(cmd.getOptionStrict("network-path"));

		AVTraceWriter writer = new AVTraceWriter(new File(cmd.getOptionStrict("output-path")));
		AVTraceListener listener = new AVTraceListener(network, writer);

		EventsManager eventsManager = EventsUtils.createEventsManager();
		eventsManager.addHandler(listener);

		EventsReaderXMLv1 reader = new EventsReaderXMLv1(eventsManager);
		reader.addCustomEventMapper("AVTransit", new AVTransitEventMapper());
		reader.readFile(cmd.getOptionStrict("events-path"));

		listener.finish();
		writer.close();
	}
}
