package trc_amod_control;

import java.util.Collection;

import org.matsim.core.config.Config;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.mobsim.qsim.AbstractQSimPlugin;
import org.matsim.core.mobsim.qsim.pt.TransitEnginePlugin;

import com.google.inject.Provides;
import com.google.inject.Singleton;

import ch.ethz.idsc.amodeus.matsim.mod.AmodeusQSimPlugin;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.framework.AVQSimPlugin;
import ch.ethz.matsim.baseline_scenario.transit.simulation.BaselineTransitPlugin;

public class DispatchingPaperQSimModule extends AbstractModule {
	@Override
	public void install() {
	}

	@Provides
	@Singleton
	public Collection<AbstractQSimPlugin> provideQSimPlugins(Config config) {
		Collection<AbstractQSimPlugin> plugins = new AVModule().provideQSimPlugins(config);
		plugins.removeIf(p -> p instanceof AVQSimPlugin);
		plugins.removeIf(p -> p instanceof TransitEnginePlugin);
		plugins.add(new AmodeusQSimPlugin(config));
		plugins.add(new BaselineTransitPlugin(config));
		return plugins;
	}

}
