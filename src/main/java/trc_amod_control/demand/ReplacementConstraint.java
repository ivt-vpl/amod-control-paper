package trc_amod_control.demand;

import java.util.BitSet;
import java.util.Collection;
import java.util.List;

import ch.ethz.matsim.mode_choice.v2.framework.ModeChoiceTrip;
import ch.ethz.matsim.mode_choice.v2.framework.tour_based.constraints.TourConstraint;
import ch.ethz.matsim.mode_choice.v2.framework.tour_based.constraints.TourConstraintFactory;
import ch.ethz.matsim.mode_choice.v2.framework.tour_based.estimation.TourCandidate;

public class ReplacementConstraint implements TourConstraint {
	private final BitSet replacementMask;

	private ReplacementConstraint(BitSet replacementMask) {
		this.replacementMask = replacementMask;
	}

	@Override
	public boolean validateAfterEstimation(TourCandidate candidate, List<TourCandidate> previousCandidates) {
		return true;
	}

	@Override
	public boolean validateBeforeEstimation(List<String> modes, List<List<String>> previousModes) {
		for (int i = 0; i < modes.size(); i++) {
			if (modes.get(i).equals("av")) {
				if (!replacementMask.get(i)) {
					return false;
				}
			}
		}

		return true;
	}

	public static class Factory implements TourConstraintFactory {
		private final Collection<String> replaceableModes;

		public Factory(Collection<String> replaceableModes) {
			this.replaceableModes = replaceableModes;
		}

		@Override
		public TourConstraint createConstraint(List<ModeChoiceTrip> trips, Collection<String> initialModes) {
			BitSet mask = new BitSet(trips.size());

			for (int i = 0; i < trips.size(); i++) {
				if (replaceableModes.contains(trips.get(i).getInitialMode())) {
					mask.set(i);
				}
			}

			return new ReplacementConstraint(mask);
		}
	}
}
