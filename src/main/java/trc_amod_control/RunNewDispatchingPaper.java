package trc_amod_control;

import java.io.File;
import java.util.Properties;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.contrib.dvrp.run.DvrpConfigGroup;
import org.matsim.contrib.dvrp.trafficmonitoring.DvrpTravelTimeModule;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import org.matsim.core.population.io.PopulationWriter;
import org.matsim.core.scenario.ScenarioUtils;

import ch.ethz.idsc.amodeus.matsim.mod.AmodeusDispatcherModule;
import ch.ethz.idsc.amodeus.matsim.mod.AmodeusModule;
import ch.ethz.idsc.amodeus.options.ScenarioOptions;
import ch.ethz.idsc.amodeus.prep.MatsimShapeFileVirtualNetworkCreator;
import ch.ethz.idsc.amodeus.virtualnetwork.VirtualNetwork;
import ch.ethz.matsim.av.framework.AVConfigGroup;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.routing.AVRoute;
import ch.ethz.matsim.av.routing.AVRouteFactory;
import ch.ethz.matsim.baseline_scenario.BaselineModule;
import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.traffic.BaselineTrafficModule;
import ch.ethz.matsim.baseline_scenario.transit.BaselineTransitModule;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRoute;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRouteFactory;
import ch.ethz.matsim.baseline_scenario.zurich.ZurichModule;
import trc_amod_control.travel_data.IterativeTravelDataModule;

public class RunNewDispatchingPaper {
	static public void main(String[] args) throws Exception {
		CommandLine cmd = new CommandLine.Builder(args) //
				.allowOptions("dispatcher", "random-seed", "fleet-size", "dispatching-interval", "rebalancing-interval",
						"distance-heuristics", "is-prepared") //
				.build();

		Config config = ConfigUtils.loadConfig(cmd.getPositionalArgumentStrict(0), new DvrpConfigGroup(),
				new AVConfigGroup());
		cmd.applyConfiguration(config);

		if (cmd.hasOption("is-prepared")) {
			config.plans().setInputFile("paper_population.xml.gz");
		}

		// Standard scenario loading

		Scenario scenario = ScenarioUtils.createScenario(config);
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(DefaultEnrichedTransitRoute.class,
				new DefaultEnrichedTransitRouteFactory());
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(AVRoute.class, new AVRouteFactory());
		ScenarioUtils.loadScenario(scenario);

		// All scenario adjustments for the dispatching paper

		File virtualNodesShpPath = new File("hexagon_2000/hexagon_2000.shp");
		File serviceAreaShpPath = new File("stadtkreis/Stadtkreis.shp");

		Network avNetwork = DispatchingPaperUtils.createAVNetwork(virtualNodesShpPath, scenario.getNetwork());

		if (!cmd.hasOption("is-prepared")) {
			DispatchingPaperUtils.cleanupScenario(scenario);
			DispatchingPaperUtils.generateDemand(scenario, avNetwork, serviceAreaShpPath);
		}

		DispatchingPaperUtils.adaptConfiguration(config, cmd);

		// Set some Amodeus options

		ScenarioOptions scenarioOptions = new ScenarioOptions(new File("."), new Properties());
		scenarioOptions.setProperty("shapeFile", virtualNodesShpPath.toString());
		scenarioOptions.setProperty("completeGraph", "true");
		scenarioOptions.setProperty("dtTravelData", "300");
		scenarioOptions.setProperty("LPSolver", "TIMEINVARIANT");

		// We need to create a virtual network and the travel data

		VirtualNetwork<Link> virtualNetwork = MatsimShapeFileVirtualNetworkCreator.createVirtualNetwork(avNetwork,
				scenarioOptions);

		// Set up the controller

		Controler controller = new Controler(scenario);

		// Modules for AV
		controller.addOverridingModule(new DvrpTravelTimeModule());
		controller.addOverridingModule(new AVModule());
		controller.addOverridingModule(new AmodeusModule());
		controller.addOverridingModule(new AmodeusDispatcherModule());

		int fleetSize = cmd.getOption("fleet-size").map(Integer::parseInt).get();
		controller.addOverridingModule(new IterativeTravelDataModule(fleetSize, scenarioOptions));

		// Baseline scenario
		controller.addOverridingModule(new BaselineModule());
		controller.addOverridingModule(new BaselineTransitModule());
		controller.addOverridingModule(new ZurichModule());
		controller.addOverridingModule(new BaselineTrafficModule(3.0));

		// Combine the QSim modules of Baseline (Transit) and AV
		controller.addOverridingModule(new DispatchingPaperQSimModule());

		// Additional for dispatching paper
		controller.addOverridingModule(
				DispatchingPaperUtils.createModule(cmd, scenario.getPopulation(), avNetwork, virtualNetwork));

		controller.run();

		if (!cmd.hasOption("is-prepared")) {
			new PopulationWriter(scenario.getPopulation()).write("paper_population.xml.gz");
		}
	}
}
