package trc_amod_control.travel_data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.PersonDepartureEvent;
import org.matsim.api.core.v01.events.handler.PersonDepartureEventHandler;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.Population;
import org.matsim.core.router.MainModeIdentifier;
import org.matsim.core.router.MainModeIdentifierImpl;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.core.router.TripStructureUtils;
import org.matsim.core.router.TripStructureUtils.Trip;
import org.matsim.pt.PtConstants;

import ch.ethz.idsc.amodeus.prep.Request;

public class RequestTracker implements PersonDepartureEventHandler {
	private final Population population;
	private final Network network;

	private final Map<Id<Person>, List<Double>> departureTimes = new HashMap<>();

	public RequestTracker(Population population, Network network) {
		this.population = population;
		this.network = network;

		for (Person person : population.getPersons().values()) {
			departureTimes.put(person.getId(), new LinkedList<>());
		}
	}

	@Override
	public void handleEvent(PersonDepartureEvent event) {
		if (event.getLegMode().equals("av")) {
			departureTimes.get(event.getPersonId()).add(event.getTime());
		}
	}

	public Set<Request> getRequestsAndReset() {
		StageActivityTypes stageActivityTypes = new StageActivityTypesImpl(PtConstants.TRANSIT_ACTIVITY_TYPE);
		MainModeIdentifier mainModeIdentifier = new MainModeIdentifierImpl();

		Set<Request> requests = new HashSet<>();

		for (Person person : population.getPersons().values()) {
			Plan plan = person.getSelectedPlan();

			Iterator<Double> departureTimeIterator = departureTimes.get(person.getId()).iterator();
			double departureTime = Double.NEGATIVE_INFINITY;

			for (Trip trip : TripStructureUtils.getTrips(plan, stageActivityTypes)) {
				if (mainModeIdentifier.identifyMainMode(trip.getTripElements()).equals("av")) {
					// - If a departure time has been tracked, use this one
					// - If not, use the one from the schedule
					// - In any case make sure that this departure is *at least* the previous
					// departure time

					departureTime = Math.max(departureTime,
							departureTimeIterator.hasNext() ? departureTimeIterator.next()
									: trip.getOriginActivity().getEndTime());

					Link startLink = network.getLinks().get(trip.getOriginActivity().getLinkId());
					Link endLink = network.getLinks().get(trip.getDestinationActivity().getLinkId());

					if (departureTime < 24.0 * 3600.0 - 1) {
						requests.add(new Request(departureTime, startLink, endLink));
					}
				}
			}

			departureTimes.get(person.getId()).clear();
		}

		return requests;
	}
}
