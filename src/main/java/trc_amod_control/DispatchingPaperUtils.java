package trc_amod_control;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.api.core.v01.population.Population;
import org.matsim.api.core.v01.population.Route;
import org.matsim.contrib.dvrp.run.DvrpModule;
import org.matsim.core.config.Config;
import org.matsim.core.config.groups.QSimConfigGroup.TrafficDynamics;
import org.matsim.core.config.groups.StrategyConfigGroup.StrategySettings;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.algorithms.NetworkCleaner;
import org.matsim.core.network.algorithms.TransportModeNetworkFilter;

import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;

import ch.ethz.idsc.amodeus.virtualnetwork.VirtualNetwork;
import ch.ethz.matsim.av.config.AVConfig;
import ch.ethz.matsim.av.config.AVDispatcherConfig;
import ch.ethz.matsim.av.config.AVGeneratorConfig;
import ch.ethz.matsim.av.config.AVOperatorConfig;
import ch.ethz.matsim.av.framework.AVConfigGroup;
import ch.ethz.matsim.av.framework.AVUtils;
import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRoute;
import trc_amod_control.demand.DemandGenerator;
import trc_amod_control.demand.ShpFilter;

public class DispatchingPaperUtils {
	static public void cleanupScenario(Scenario scenario) {
		for (Person person : scenario.getPopulation().getPersons().values()) {
			Set<Plan> plansToRemove = new HashSet<>(person.getPlans());
			plansToRemove.remove(person.getSelectedPlan());
			plansToRemove.forEach(person::removePlan);

			Plan plan = person.getSelectedPlan();

			for (PlanElement element : plan.getPlanElements()) {
				if (element instanceof Leg) {
					Leg leg = (Leg) element;

					if (leg.getMode().equals("pt")) {
						Route route = leg.getRoute();

						if (route != null && !route.getRouteType().equals(DefaultEnrichedTransitRoute.ROUTE_TYPE)) {
							leg.setRoute(null);
						}
					}
				}
			}
		}
	}

	static public void generateDemand(Scenario scenario, Network avNetwork, File serviceAreaShpPath)
			throws IOException, InterruptedException {
		int totalNumberOfPersons = scenario.getPopulation().getPersons().size();
		int numberOfThreads = scenario.getConfig().global().getNumberOfThreads();

		Collection<Id<Link>> serviceAreaLinkIds = ShpFilter.create(serviceAreaShpPath).filter(avNetwork);

		Random random = new Random(0);
		List<Integer> seeds = new ArrayList<>(totalNumberOfPersons);

		for (int i = 0; i < totalNumberOfPersons; i++) {
			seeds.add(random.nextInt(Integer.MAX_VALUE));
		}

		Iterator<? extends Person> personIterator = scenario.getPopulation().getPersons().values().iterator();
		Iterator<Integer> seedIterator = seeds.iterator();

		List<Thread> threads = new ArrayList<>(numberOfThreads);
		AtomicInteger numberOfProcessedPersons = new AtomicInteger(0);

		for (int i = 0; i < numberOfThreads; i++) {
			threads.add(new Thread(() -> {
				DemandGenerator generator = new DemandGenerator(serviceAreaLinkIds);

				while (true) {
					Person person = null;
					int seed = 0;

					synchronized (personIterator) {
						if (personIterator.hasNext()) {
							person = personIterator.next();
							seed = seedIterator.next();
						}
					}

					if (person != null) {
						generator.apply(seed, person);
						numberOfProcessedPersons.incrementAndGet();
					} else {
						return;
					}
				}
			}));
		}

		threads.add(new Thread(() -> {
			try {
				while (numberOfProcessedPersons.get() < totalNumberOfPersons) {
					System.out.println(String.format("%d / %d (%.2f%%) Generating demand",
							numberOfProcessedPersons.get(), totalNumberOfPersons,
							100.0 * numberOfProcessedPersons.get() / totalNumberOfPersons));
					Thread.sleep(1000);
				}
			} catch (InterruptedException e) {
			}
		}));

		threads.forEach(Thread::start);

		for (Thread thread : threads) {
			thread.join();
		}

		Iterator<? extends Person> removePersonIterator = scenario.getPopulation().getPersons().values().iterator();

		while (removePersonIterator.hasNext()) {
			Person person = removePersonIterator.next();
			Plan plan = person.getSelectedPlan();

			boolean hasAVLeg = false;

			for (PlanElement element : plan.getPlanElements()) {
				if (element instanceof Leg) {
					Leg leg = (Leg) element;

					if (leg.getMode().equals("av")) {
						hasAVLeg = true;
						break;
					}
				}
			}

			if (!hasAVLeg) {
				removePersonIterator.remove();
			}
		}
	}

	static public Network createAVNetwork(File virtualNetworkShpPath, Network network) {
		try {
			Network filterNetwork = NetworkUtils.createNetwork();
			new TransportModeNetworkFilter(network).filter(filterNetwork, Collections.singleton("car"));
			Collection<Id<Link>> avLinkIds = ShpFilter.create(virtualNetworkShpPath).filter(filterNetwork);

			Set<Id<Link>> removableIds = new HashSet<>(filterNetwork.getLinks().keySet());
			removableIds.removeAll(avLinkIds);

			removableIds.forEach(id -> filterNetwork.removeLink(id));
			new NetworkCleaner().run(filterNetwork);

			return filterNetwork;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	static public void adaptConfiguration(Config config, CommandLine cmd) throws ConfigurationException {
		String dispatcherName = cmd.getOptionStrict("dispatcher");
		int fleetSize = cmd.getOption("fleet-size").map(Integer::parseInt).get();
		int randomSeed = cmd.getOption("random-seed").map(Integer::parseInt).orElse(1234);

		String outputPrefix = "";

		if (System.getenv().containsKey("SCRATCH")) {
			outputPrefix = System.getenv("SCRATCH") + "/";
		}

		String outputPath = String.format(outputPrefix + "disp_%s_%d_%d", dispatcherName, fleetSize, randomSeed);

		config.controler().setLastIteration(0);
		config.controler().setWriteEventsInterval(1);
		config.controler().setOutputDirectory(outputPath);

		if (dispatcherName.equals("FeedforwardFluidicRebalancingPolicy")) {
			config.controler().setLastIteration(1);
		}

		AVConfigGroup avConfigGroup = (AVConfigGroup) config.getModules().get("av");
		avConfigGroup.setParallelRouters(config.global().getNumberOfThreads());

		config.global().setRandomSeed(randomSeed);

		config.qsim().setFlowCapFactor(1000.0); // Freespeed
		config.qsim().setStorageCapFactor(1.0);
		config.qsim().setTrafficDynamics(TrafficDynamics.queue);

		// TODO: Adjust replanning strategies

		config.strategy().clearStrategySettings();

		StrategySettings strategy = new StrategySettings();
		strategy.setStrategyName("KeepLastSelected");
		strategy.setWeight(1.0);
		config.strategy().addStrategySettings(strategy);

		strategy = new StrategySettings();
		strategy.setStrategyName("SubtourModeChoice");
		strategy.setWeight(0.0);
		strategy.setDisableAfter(0);
		config.strategy().addStrategySettings(strategy);

		config.planCalcScore().getOrCreateModeParams("av");
	}

	static public AbstractModule createModule(CommandLine cmd, Population population, Network avNetwork,
			VirtualNetwork<Link> virtualNetwork) throws ConfigurationException {
		String dispatcherName = cmd.getOptionStrict("dispatcher");
		int fleetSize = cmd.getOption("fleet-size").map(Integer::parseInt).get();

		AVConfig avConfig = new AVConfig();
		AVOperatorConfig operatorConfig = avConfig.createOperatorConfig("av");

		AVDispatcherConfig dispatcherConfig = operatorConfig.createDispatcherConfig(dispatcherName);
		dispatcherConfig.addParam("publishPeriod", "-1");
		dispatcherConfig.addParam("dispatchPeriod", cmd.getOption("dispatching-interval").orElse("60"));
		dispatcherConfig.addParam("rebalancingPeriod", cmd.getOption("rebalancing-interval").orElse("300"));
		dispatcherConfig.addParam("distanceHeuristics", cmd.getOption("distance-heuristics").orElse("EUCLIDEAN"));

		AVGeneratorConfig generatorConfig = operatorConfig.createGeneratorConfig("DispatchingPaper");
		generatorConfig.setNumberOfVehicles(fleetSize);

		operatorConfig.createPriceStructureConfig();

		DispatchingPaperGenerator.Factory generatorFactory = new DispatchingPaperGenerator.Factory(population,
				avNetwork);

		return new AbstractModule() {
			@Override
			public void install() {
				AVUtils.bindGeneratorFactory(binder(), "DispatchingPaper").toInstance(generatorFactory);
				bind(AVConfig.class).toInstance(avConfig);

				bind(new TypeLiteral<VirtualNetwork<Link>>() {
				}).toInstance(virtualNetwork);
				bind(Key.get(Network.class, Names.named(DvrpModule.DVRP_ROUTING))).toInstance(avNetwork);
			}
		};
	}
}
