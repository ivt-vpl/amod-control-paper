package trc_amod_control.demand;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.core.population.PopulationUtils;
import org.matsim.core.router.MainModeIdentifier;
import org.matsim.core.router.MainModeIdentifierImpl;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.core.router.TripStructureUtils;
import org.matsim.core.router.TripStructureUtils.Trip;
import org.matsim.pt.PtConstants;

import ch.ethz.matsim.mode_choice.v2.framework.DefaultModeChoiceTrip;
import ch.ethz.matsim.mode_choice.v2.framework.ModeChoiceTrip;
import ch.ethz.matsim.mode_choice.v2.framework.tour_based.constraints.TourConstraint;
import ch.ethz.matsim.mode_choice.v2.framework.utils.DefaultModeChainGenerator;
import ch.ethz.matsim.mode_choice.v2.framework.utils.ModeChainGenerator;

public class DemandGenerator {
	private final static List<List<String>> previousTours = Collections.emptyList();

	private final int maximumNumberOfTrips = 10;

	private final Collection<String> availableModes = Arrays.asList("car", "pt", "bike", "walk", "av");
	private final Collection<String> constrainedModes = Arrays.asList("car", "bike");
	private final Collection<String> replaceableModes = Arrays.asList("car", "bike");

	private final StageActivityTypes stageActivityTypes = new StageActivityTypesImpl(PtConstants.TRANSIT_ACTIVITY_TYPE);
	private final MainModeIdentifier mainModeIdentifier = new MainModeIdentifierImpl();

	private final VehicleConstraint.Factory vehicleConstraintFactory;
	private final ServiceAreaConstraint.Factory serviceAreaConstraintFactory;
	private final ReplacementConstraint.Factory replacementConstraintFactory;

	public DemandGenerator(Collection<Id<Link>> serviceAreaLinkIds) {
		this.vehicleConstraintFactory = new VehicleConstraint.Factory(constrainedModes);
		this.serviceAreaConstraintFactory = new ServiceAreaConstraint.Factory(serviceAreaLinkIds);
		this.replacementConstraintFactory = new ReplacementConstraint.Factory(replaceableModes);
	}

	public void apply(int seed, Person person) {
		Random random = new Random(seed);

		for (int i = 0; i < 1000; i++) {
			random.nextDouble();
		}

		if (person.getPlans().size() > 1) {
			throw new IllegalStateException("Expected only one plan");
		}

		Plan plan = person.getPlans().get(0);
		List<Trip> trips = TripStructureUtils.getTrips(plan, stageActivityTypes);

		if (trips.size() <= maximumNumberOfTrips) {
			List<ModeChoiceTrip> modeChoiceTrips = trips.stream().map(t -> new DefaultModeChoiceTrip(person, trips, t,
					mainModeIdentifier.identifyMainMode(t.getTripElements()))).collect(Collectors.toList());
			List<String> initialModes = trips.stream()
					.map(t -> mainModeIdentifier.identifyMainMode(t.getTripElements())).collect(Collectors.toList());

			ModeChainGenerator generator = new DefaultModeChainGenerator(availableModes, trips.size());

			TourConstraint vehicleConstraint = vehicleConstraintFactory.createConstraint(modeChoiceTrips, initialModes);
			TourConstraint serviceAreaConstraint = serviceAreaConstraintFactory.createConstraint(modeChoiceTrips,
					initialModes);
			TourConstraint replacementConstraint = replacementConstraintFactory.createConstraint(modeChoiceTrips,
					initialModes);

			List<List<String>> bestConfigurations = new LinkedList<>();
			int bestNumberOfAVTrips = 0;

			while (generator.hasNext()) {
				List<String> modes = generator.next();

				if (vehicleConstraint.validateBeforeEstimation(modes, previousTours)
						&& serviceAreaConstraint.validateBeforeEstimation(modes, previousTours)
						&& replacementConstraint.validateBeforeEstimation(modes, previousTours)) {
					int numberOfAVTrips = (int) modes.stream().filter(m -> m.equals("av")).count();

					if (numberOfAVTrips > bestNumberOfAVTrips) {
						bestConfigurations.clear();
						bestConfigurations.add(modes);
					} else if (numberOfAVTrips == bestNumberOfAVTrips && numberOfAVTrips > 0) {
						bestConfigurations.add(modes);
					}
				}
			}

			if (bestConfigurations.size() > 0) {
				List<String> bestModes = bestConfigurations.get(random.nextInt(bestConfigurations.size()));

				for (int i = 0; i < trips.size(); i++) {
					if (bestModes.get(i).equals("av")) {
						plan.getPlanElements().removeIf(trips.get(i).getTripElements()::contains);

						int insertIndex = plan.getPlanElements().indexOf(trips.get(i).getOriginActivity()) + 1;
						plan.getPlanElements().add(insertIndex, PopulationUtils.createLeg("av"));
					}
				}

			}
		}
	}
}
