package trc_amod_control.analysis.price;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.events.EventsReaderXMLv1;
import org.matsim.core.events.EventsUtils;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.core.utils.io.UncheckedIOException;

import ch.ethz.matsim.av_cost_calculator.CostCalculator;
import ch.ethz.matsim.av_cost_calculator.CostCalculatorExecutor;
import ch.ethz.matsim.av_cost_calculator.run.AVValidator;
import ch.ethz.matsim.av_cost_calculator.run.AnyAVValidator;
import ch.ethz.matsim.av_cost_calculator.run.ParameterBuilder;
import ch.ethz.matsim.av_cost_calculator.run.PricingAnalysisHandler;
import ch.ethz.matsim.av_cost_calculator.run.SingleOccupancyAnalysisHandler;
import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;

public class CalculatePrice {
	static public void main(String[] args) throws UncheckedIOException, ConfigurationException, IOException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("events-path", "network-path", "output-path") //
				.allowOptions("total-time") //
				.build();

		Network network = NetworkUtils.createNetwork();
		new MatsimNetworkReader(network).readFile(cmd.getOptionStrict("network-path"));

		double totalTime = cmd.getOption("total-time").map(Double::parseDouble).orElse(30.0 * 3600.0);

		URL sourceURL = CostCalculator.class.getClassLoader().getResource("ch/ethz/matsim/av_cost_calculator/");

		File workingDirectory = new File(FileUtils.getTempDirectory(), "av_cost_calculator");
		workingDirectory.mkdirs();

		AVValidator validator = new AnyAVValidator();
		CostCalculatorExecutor executor = new CostCalculatorExecutor(workingDirectory, sourceURL);
		PricingAnalysisHandler handler = new SingleOccupancyAnalysisHandler(network, validator, totalTime);

		EventsManager eventsManager = EventsUtils.createEventsManager();
		eventsManager.addHandler(handler);

		EventsReaderXMLv1 reader = new EventsReaderXMLv1(eventsManager);
		reader.readFile(cmd.getOptionStrict("events-path"));

		Map<String, String> parameters = new ParameterBuilder(0.1, 24.0 * 3600.0, "Solo").build(handler);
		double price = executor.computePricePerPassengerKm(parameters);

		BufferedWriter writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(new File(cmd.getOptionStrict("output-path")))));
		writer.write(String.format("%f", price));
		writer.close();
	}
}
